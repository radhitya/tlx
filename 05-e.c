#include <stdio.h>
int main() {
    int a;
    scanf("%d", &a);

    if (1 <= a && a < 10) {
        printf("satuan");
    }
    else if (10 <= a && a <= 99) {
        printf("puluhan");
    }
    else if (100 <= a && a <= 999) {
        printf("ratusan");
    }
    else if (1000 <= a && a <= 9999) {
        printf("ribuan");
    }
    else if (10000 <= a && a <= 99999) {
        printf("puluhribuan");
    }
    return 0;
}

