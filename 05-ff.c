#include <stdio.h>

int main() {
    float n;
    scanf("%f", &n);

    int floor_value, ceiling_value;
    
    if (n >= 0) {
        floor_value = (int)n;
        ceiling_value = floor_value + 1;
    } else {
        ceiling_value = (int)n;
        floor_value = ceiling_value - 1;
    }

    printf("%d %d\n", floor_value, ceiling_value);

    return 0;
}

